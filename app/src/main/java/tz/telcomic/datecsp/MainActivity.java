package tz.telcomic.datecsp;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.datecs.BuildInfo;
import com.datecs.printer.Printer;
import com.datecs.printer.ProtocolAdapter;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import java.io.IOException;
import java.util.UUID;
import java.util.logging.Logger;

public class MainActivity extends AppCompatActivity implements Runnable {
     private Button print,scan,disable;
    String[] products= new String[]{"Electronics Equipments"};
    private BluetoothAdapter mBluetoothAdapter;
    private ProtocolAdapter mprotocolAdp;
    private ProtocolAdapter.Channel mprinterChannelAdapter;
    private UUID applicationUUID = UUID
            .fromString("00001101-0000-1000-8000-00805F9B34FB");
    private String name="O9VFDWEBAPI-10131757710927293010TZ100833";
    private BluetoothSocket mBluetoothSocket;
    private static final int REQUEST_CONNECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;
    private BluetoothDevice mBluetoothDevice;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //printer=new Printer()
        initializeVAriable();
        setonClickListerner();
    }



    public void initializeVAriable(){
        print=findViewById(R.id.print);
        disable=findViewById(R.id.disable);
        scan=findViewById(R.id.scan);
    }

    public void setonClickListerner(){
        print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Thread t = new Thread() {
                    public void run() {
                        try {
                            ProtocolAdapter.setDebug(true);
                            mprotocolAdp= new ProtocolAdapter(mBluetoothSocket.getInputStream(),mBluetoothSocket.getOutputStream());
                            if(mprotocolAdp.isProtocolEnabled()){
                            mprinterChannelAdapter=mprotocolAdp.getChannel(ProtocolAdapter.CHANNEL_PRINTER);
                            Printer printer=new Printer(mprinterChannelAdapter.getInputStream(), mprinterChannelAdapter.getOutputStream());
                            StringBuffer stringBuffer=new StringBuffer();
                            StringBuilder sb= new StringBuilder();
                            String BILL ="\n";
                            Drawable drawable=getDrawable(R.drawable.lg);
                            Bitmap bitmapss = ((BitmapDrawable)drawable).getBitmap();
                            final int width= bitmapss.getWidth();
                            final int height= bitmapss.getHeight();
                            final int [] argb= new int[width*height];
                            bitmapss.getPixels(argb,0,width,0,0,width,height);
                            bitmapss.recycle();
                            printer.reset();
                            printer.setIntensity(3);
                            printer.setAlign(Printer.ALIGN_LEFT);
//                          printer.printCompressedImage(argb,width,height,Printer.ALIGN_LEFT,false);
                            printer.reset();
                            printer.selectPageMode();
                            printer.setPageRegion(0, 0, 320, 160, Printer.PAGE_LEFT);
                            printer.setPageXY(0, 4);
                            printer.printCompressedImage(argb,width,height,Printer.ALIGN_LEFT,false);
                            printer.setPageRegion(50, 0, 320, 160, Printer.PAGE_LEFT);
                            printer.setPageXY(30, 30);
                            printer.printTaggedText("START OF LEGAL RECEIPT ***");
                            printer.printPage();
                            printer.setLineSpace(28);
                            printer.selectStandardMode();
                            printer.flush();
                            Bitmap bitmaps = generateQrcode();
                            final int width2= bitmaps.getWidth();
                            final int height2= bitmaps.getHeight();
                            final int [] argb2= new int[width2*height2];
                            bitmaps.getPixels(argb2,0,width2,0,0,width2,height2);
                            bitmaps.recycle();
                            String BIlls="\n";
                                        //=BILL+AlignCenter("*** START OF LEGAL RECEIPT ***\n");
                            BILL=BIlls+
                            AlignCenter("ADVATECH COMPANY LIMITED")+
                            AlignCenter("TIN: "+"109272930")+
                            AlignCenter("VRN: "+"40033468U")+
                            AlignCenter("SERIAL NUMBER: "+"10TZ100833")+
                            AlignCenter("UIN: "+name.split("-")[0]+"-")+AlignCenter(name.split("-")[1])+
                            AlignCenter("TAX OFFICE: "+"ILALA") +
                            AlignCenter("    ")+
                            spacing("CUST NAME:","Michael Philemon")+
                            spacing("CUST ID TYPE:","TIN")+
                            spacing("CUST ID:","109272930")+
                            spacing("PHONE NUMBER:","0672475563")+
                            AlignCenter("--------------------------------")+
                            spacing("RECEIPT NUMBER:","87512")+
                            spacing("ZNO:","2/20020401")+
                            spacing("DATE:12-10-2020","TIME: 12:12:10")+
                            AlignCenter("--------------------------------");
                            for(String product :products){
                               BILL=BILL +AlignLeft(product)+spacing("2 x 6,000,000","12,000,000 A");
                            }
                            BILL=BILL+AlignCenter("--------------------------------")+
                            spacing("TOTAL EXCL OF TAX:","10,169,491.52")+
                            AlignCenter("--------------------------------")+
                            spacing("TOTAL TAX:","1,830,508.47")+
                            AlignCenter("--------------------------------")+
                            spacing("TOTAL INCL OF TAX:","12,000,000")+
                            AlignCenter("--------------------------------")+
                            spacing("CASH:","12,000,000")+
                            AlignCenter("--------------------------------")+
                            AlignCenter("RECEIPT VERIFICATION CODE");
                            printer.printTaggedText(BILL);
                            printer.printTaggedText(" \n");
                            printer.printTaggedText( AlignCenter("{reset}{center}{b}{w}"+"DFRSAD87512"+"{reset}"));
                            printer.printTaggedText(" ");
//                                printer.setBarcode(Printer.ALIGN_CENTER,false,2,Printer.HRI_BELOW,100);
//                                printer.printQRCode(6,1,"0672475563");
                            printer.printCompressedImage(argb2,width2,height2,Printer.ALIGN_CENTER,true);
                            String endOfLeg=AlignCenter("*** END OF LEGAL RECEIPT ***");
                            printer.printTaggedText(endOfLeg);
                            printer.feedPaper(110);
                            printer.cutPaper();
                            printer.close();
                            }
                            else{
                                Log.e("MainActivity", "protocol mode disabled ");

                            }
                        } catch (Exception e) {
                            Log.e("MainActivity", "Exe ", e);
                        }
                    }
                };
                t.start();
            }
        });

        scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                if (mBluetoothAdapter == null) {
                    Toast.makeText(MainActivity.this, "Message1", Toast.LENGTH_SHORT).show();
                } else {
                    if (!mBluetoothAdapter.isEnabled()) {
                        Intent enableBtIntent = new Intent(
                                BluetoothAdapter.ACTION_REQUEST_ENABLE);
                        startActivityForResult(enableBtIntent,
                                REQUEST_ENABLE_BT);
                    } else {
                        //ListPairedDevices();
                        Intent connectIntent = new Intent(MainActivity.this,
                                DeviceListActivity.class);
                        startActivityForResult(connectIntent,
                                REQUEST_CONNECT_DEVICE);
                    }
                }
            }
        });


        disable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBluetoothAdapter.disable();
            }
        });

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQUEST_CONNECT_DEVICE:
                if (resultCode == Activity.RESULT_OK) {
                    Bundle mExtra = data.getExtras();
                    String mDeviceAddress = mExtra.getString("DeviceAddress");
                    Log.v("TAG", "Coming incoming address " + mDeviceAddress);
                    mBluetoothDevice = mBluetoothAdapter
                            .getRemoteDevice(mDeviceAddress);

//                    mBluetoothConnectProgressDialog = ProgressDialog.show(this,
//                            "Connecting...", mBluetoothDevice.getName() + " : "
//                                    + mBluetoothDevice.getAddress(), true, false);
                    Thread mBlutoothConnectThread = new Thread(this);
                    mBlutoothConnectThread.start();
                    // pairToDevice(mBluetoothDevice); This method is replaced by
                    // progress dialog with thread
                }
                break;

            case REQUEST_ENABLE_BT:
                if (resultCode == Activity.RESULT_OK) {
                   // ListPairedDevices();
                    Intent connectIntent = new Intent(MainActivity.this,
                            DeviceListActivity.class);
                    startActivityForResult(connectIntent, REQUEST_CONNECT_DEVICE);
                } else {
                    Toast.makeText(MainActivity.this, "Message", Toast.LENGTH_SHORT).show();
                }
                break;
        }

    }

    @Override
    public void run() {

        try {

            mBluetoothSocket = mBluetoothDevice
                    .createRfcommSocketToServiceRecord(applicationUUID);
            mBluetoothAdapter.cancelDiscovery();
            mBluetoothSocket.connect();
            mHandler.sendEmptyMessage(0);
        } catch (IOException eConnectException) {
            Log.d("TAG", "CouldNotConnectToSocket", eConnectException);
            closeSocket(mBluetoothSocket);
            return;
        }

    }
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Toast.makeText(MainActivity.this, "DeviceConnected", Toast.LENGTH_SHORT).show();
        }
    };

    private void closeSocket(BluetoothSocket nOpenSocket) {
        try {
            nOpenSocket.close();
            Log.d("TAG", "SocketClosed");
        } catch (IOException ex) {
            Log.d("TAG", "CouldNotCloseSocket");
        }
    }

    public String byteArrayToHexString(byte[] buffer){
        char[] tmp=new char[buffer.length*3];
        for(int i=0,j=0;i< buffer.length;i++){
            int a=(buffer[i]& 0xff)>>4;
            int b=(buffer[i]& 0x0f);
            tmp[j++]=(char)(a<10?'0'+ a :'A'+a-10 );
            tmp[j++]=(char)(b<10?'0'+ a :'A'+b-10 );
            tmp[j++] = ' ';
        }
        return new String(tmp);
    }


    public String  AlignRight(String text){
        int result=31-text.length();
        int average=result/2;
        String space="";
        for(int a=0; a<average;a++){
            space=space+" ";
        }
        return space+space+text+"\n";
    }
    public String  AlignLeft(String text){
        int result=31-text.length();
        int average=result/2;
        String space="";
        for(int a=0; a<average;a++){
            space=space+" ";
        }
        return text+space+space+"\n";
    }

    public String  AlignCenter(String text){
        int result=31-text.length();
        int result2=32-text.length();

        if(result%2==0){
            int average=result/2;
            String space="";
            for(int a=0; a<average;a++){
                space=space+" ";
            }
            return space+text+space+"\n";
        }
        else{
            Double average=(result2/2)-0.5;
            Double d=new Double(average);
            int i=d.intValue();
            String space="";
            for(int a=0;a<i;a++){
                space=space+" ";
            }
            return space+text+space+"\n";
        }
    }


    public String spacing(String leftText,String rightText){
        int result= 32-(leftText.length()+rightText.length());
        Log.d("length",""+leftText.length());
        String space="";
        for(int i=0;i<result;i++){
            space=space+" ";
        }
        return leftText +space +rightText+"\n";
    }


        public Bitmap generateQrcode() throws WriterException {
        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        BitMatrix bitMatrix = qrCodeWriter.encode("https://verify.tra.go.tz/A37C2F138900", BarcodeFormat.QR_CODE, 240, 240);
        Bitmap bitmap = Bitmap.createBitmap(240, 240, Bitmap.Config.ARGB_8888);
        for (int x = 0; x<240; x++){
            for (int y=0; y<240; y++){
                bitmap.setPixel(x,y,bitMatrix.get(x,y)? Color.BLACK : Color.WHITE);

            }
        }
       return  bitmap;

    }

}